import 'package:finalipc/model/product_modelData.dart';
import 'package:finalipc/pages/first_page.dart';
import 'package:finalipc/pages/profile_page.dart';
import 'package:finalipc/pages/register_page.dart';
import 'package:flutter/material.dart';
import 'package:finalipc/pages/login_page.dart';
import 'package:finalipc/pages/list_products_page.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  final routes = <String, WidgetBuilder>{
    LoginPage.tag: (context) => LoginPage(),
    ListProductsPage.tag: (context) => ListProductsPage(),
    FirstPage.tag: (context) => FirstPage(),
    RegisterPage.tag: (context) => RegisterPage(),
    ProfilePage.tag: (context) => ProfilePage(),
  };

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Buy right now',
      theme: ThemeData(
        primarySwatch: Colors.lightBlue,
        fontFamily: 'Arial',
      ),
      home: FirstPage(),
      routes: routes,
    );
  }
}