import 'package:finalipc/pages/list_products_page.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:finalipc/model/product_modelData.dart';

// ignore: must_be_immutable, camel_case_types
class shoppingCar extends StatelessWidget{
  static String tag = 'shoppingCar';
  final Product product;
  List<Product> allProducts;
  double totalAmount = 0;
  shoppingCar({@required this.product, @required this.allProducts});

  void add(Product addedProduct){
    allProducts.add(addedProduct);
    print("yess $addedProduct");
  }

  double getTotal(){
    allProducts.forEach((element) {totalAmount += element.price;});
    return totalAmount;
  }

  @override
  Widget build(BuildContext context) {

    add(product);

    return Scaffold(
      body: SafeArea(
        child: new Column(
          children: [
            Center(child: Icon(Icons.shopping_bag),),
            Container(height: 500,width: 500,child:  ListView.separated(
              itemBuilder: (context,i){return GestureDetector(
                child: ListTile(
                  title: Text("${allProducts[i].name}"),
                  subtitle: Text("${allProducts[i].price}"),
                ),
              );},
              separatorBuilder: (_,i) => Divider(thickness: 1, color: Colors.grey,),
              itemCount: allProducts.length,
            ),),
            Text("${getTotal()}€", textScaleFactor: 3,),
            Center(child: ElevatedButton(
                child: Text("Done"),
              onPressed: () => {print(allProducts.length),
                  Navigator.push(context, MaterialPageRoute(builder: (context) => ListProductsPage.withList(allProducts)))
              },
            ),
            )


          ]
        ),
      )
    );

  }

  }

