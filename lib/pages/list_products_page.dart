import 'package:finalipc/pages/login_page.dart';
import 'package:finalipc/pages/profile_page.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'dart:async';
import 'package:finalipc/pages/shoppingCar.dart';
import 'package:finalipc/model/product_modelData.dart';
import 'dart:convert';

import 'details_page.dart';

// ignore: must_be_immutable
class ListProductsPage extends StatelessWidget {
  static String tag = 'home-page';
  final Product empty = new Product(name: "",description: "",price: 0,image: "",);
  List<Product> addedProducts = [];

  ListProductsPage();

  ListProductsPage.withList(List<Product> added){
    this.addedProducts = added;
  }

  List<Product> getAddedProducts(){
    return addedProducts;
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      endDrawer: Drawer(
        child: ListView(
          // Important: Remove any padding from the ListView.
          padding: EdgeInsets.zero,
          children: <Widget>[
            DrawerHeader(
              child: Column(
                children: [
                  Spacer(flex:1),
                  Text('PC CON PONENTES',textScaleFactor: 2,style: TextStyle(fontFamily: "Courier",color: Colors.white),),
                  Spacer(flex:1),
                ],
              ),
              decoration: BoxDecoration(
                color: Colors.indigo,
              ),
            ),
            ListTile(
              title: Text('Mi Perfil', textAlign: TextAlign.center,),
              onTap: (){
                Navigator.of(context).pushNamed(ProfilePage.tag);
              },
            ),
            ListTile(
              title: Text('Mis Pedidos', textAlign: TextAlign.center,),
              onTap: () { showDialog(
                    context: context, builder: (context)=> AlertDialog(
                  title: Text("Todavía no tienes ningún pedido."),
                  content: Center(child: Icon(Icons.no_encryption_gmailerrorred_outlined)),
                  // ignore: deprecated_member_use
                  actions: [FlatButton(onPressed: (){
                    Navigator.of(context).pop();
                  }, child: Text("Aceptar")),
                  ],
                ),
              );},
            ),
            ListTile(
              title: Text('Opciones', textAlign: TextAlign.center,),
              onTap: () {},
            ),
            ListTile(
              title: Text('Cerrar sesión', textAlign: TextAlign.center,),
              onTap: () {Navigator.push(context, MaterialPageRoute(builder: (context) => LoginPage()));},
            ),
            ListTile(
              title: Text('🔙', textAlign: TextAlign.center, style: TextStyle(fontSize: 30),),
              onTap: () {
                Navigator.pop(context);
              },
            ),Divider(),
      ListTile(
        title: Text('Rate Us', textAlign: TextAlign.center,),
        subtitle: Text("⭐⭐⭐⭐⭐", textAlign: TextAlign.center,),
        onTap: () {},)
          ],
        ),
      ),

      appBar: AppBar(
        backgroundColor: Colors.indigo,
        automaticallyImplyLeading: false,
        title: Row(
          children: [
            IconButton(
                padding: EdgeInsets.only(left: 10),
                alignment: Alignment.centerLeft,
                icon: Icon(
                  Icons.search_sharp,
                  color: Colors.white,
                  size: 35,
                ),
                onPressed: () {
                  //code to execute when this button is pressed
                }),
            SizedBox(
              width: 10,
            ),
            IconButton(
                padding: EdgeInsets.only(left: 10),
                alignment: Alignment.centerLeft,
                icon: Icon(
                  Icons.shopping_cart_sharp,
                  color: Colors.white,
                  size: 35,
                ),
                onPressed: () {
                  Navigator.push(context, MaterialPageRoute(builder: (context) => shoppingCar(product: empty,allProducts:addedProducts,)));
                }),
            SizedBox(
              width: 10,
            ),
          ],
        ),
        actions: [
          Row(
            children: [
              Builder(
                builder: (context) => IconButton(
                  padding: EdgeInsets.only(right: 10),
                  alignment: Alignment.centerLeft,
                  icon: Icon(
                    Icons.person_sharp,
                    color: Colors.white,
                    size: 35,
                  ),
                  onPressed: () => Scaffold.of(context).openEndDrawer(),
                ),
              ),
            ],
          ),
        ],
      ),

      body: ProductList(addedProducts),
    );
  }
}

// ignore: must_be_immutable
class ProductList extends StatelessWidget {

  List<Product> addedItems;

  ProductList(List<Product> added){
    this.addedItems = added;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: EdgeInsets.all(10),
        child: FutureBuilder(
          future: downloadData(context),
          builder: (context, snapshot) {
            if (!snapshot.hasData)
              return Center(child: CircularProgressIndicator());

            Map<String, dynamic> data = json.decode(snapshot.data);
            var products = Products.fromJson(data);

            return GridView.builder(
              gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
                  maxCrossAxisExtent: 200,
                  childAspectRatio: 1,
                  crossAxisSpacing: 7,
                  mainAxisSpacing: 10),
              itemCount: products.products.length,
              itemBuilder: (context, i) {
                return Container(
                  //padding: EdgeInsets.all(5),
                  alignment: Alignment.center,
                  decoration: BoxDecoration(boxShadow: [
                    BoxShadow(
                      color: Colors.white,
                    )
                  ], borderRadius: BorderRadius.circular(15)),
                  child: _ItemListTile(
                    productName: products.products[i].name,
                    price: products.products[i].price,
                    imagePath: products.products[i].image,
                    onTap: () {
                      Navigator.of(context).push(MaterialPageRoute(
                          builder: (context) => ProductDetailsPage(
                                product: products.products[i], added: addedItems ,
                              )));
                    },
                  ),
                );
              },
            );
          },
        ));
  }
}

class _ItemListTile extends StatelessWidget {
  final String productName;
  final double price;
  final String imagePath;
  final Function onTap;

  _ItemListTile({this.productName, this.price, this.imagePath, this.onTap});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 170,
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.all(Radius.circular(10)),
          boxShadow: [
            BoxShadow(
              color: Colors.black,
            )
          ]),
      child: Column(
        children: [
          ListTile(
            title: Text("${this.productName}"),
            subtitle: Text("${this.price}",style: TextStyle(color: Colors.red),),
            contentPadding: EdgeInsets.all(10),
            shape: RoundedRectangleBorder(),
            tileColor: Colors.white,
            focusColor: Colors.blue,
            selectedTileColor: Colors.green,
            hoverColor: Colors.yellow,
            onTap: this.onTap,
          ),
            GestureDetector(
              onTap: this.onTap,
              child: Container(
                width: 200,height: 78,
                child: Hero(
                    tag: this.productName,
                    child: Image.asset("${this.imagePath}")),
              ),
            ),

          ],
      ),
    );
  }
}

Future<String> downloadData(BuildContext context) {
  return Future.delayed(Duration(seconds: 2), () {
    return DefaultAssetBundle.of(context)
        .loadString("assets/database/products.json");
  });
}
