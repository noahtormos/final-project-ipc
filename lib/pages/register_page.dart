import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'list_products_page.dart';

class RegisterPage extends StatefulWidget {
  static String tag = 'register_page';

  @override
  _RegisterPageState createState() => new _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  TextEditingController _name;
  TextEditingController _lastname;
  TextEditingController _number;
  TextEditingController _email;
  TextEditingController _genre;

  @override
  void initState() {
    super.initState();
    _name = TextEditingController();
    _lastname = TextEditingController();
    _number = TextEditingController();
    _email = TextEditingController();
    _genre = TextEditingController();
  }

  @override
  void dispose() {
    _name.dispose();
    _lastname.dispose();
    _number.dispose();
    _email.dispose();
    _genre.dispose();

    super.dispose();
  }

  String gender = 'male';

  @override
  Widget build(BuildContext context) {
    var gender;
    return Scaffold(
        body: SafeArea(
      child: SingleChildScrollView(
        child: Container(
          width: double.maxFinite,
          height: double.maxFinite,
          child: Column(
            children: [
              Container(
                  padding: EdgeInsets.only(top: 30, left: 15, right: 15),
                  height: 800,
                  child: SizedBox(
                    width: double.maxFinite,
                    height: double.infinity,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        CircleAvatar(
                          backgroundColor: Colors.transparent,
                          radius: 48.0,
                          child: Image.asset("assets/img/logo.png"),
                        ),
                        Divider(),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Container(
                              padding: EdgeInsets.only(right: 10),
                              width: 200,
                              child: TextFormField(
                                  controller: _name,
                                  decoration: InputDecoration(
                                      icon: Icon(Icons.person,
                                          color: Colors.indigo),
                                      hintText: "Nombre",
                                      contentPadding: EdgeInsets.all(10.0),
                                      border: OutlineInputBorder(
                                          borderRadius:
                                              BorderRadius.circular(20.0))),
                                  onEditingComplete: () {
                                    setState(() {});
                                  },
                                  keyboardType: TextInputType.name),
                            ),
                            Container(
                              padding: EdgeInsets.only(right: 10),
                              width: 180,
                              child: TextFormField(
                                controller: _lastname,
                                decoration: InputDecoration(
                                    hintText: "Apellidos",
                                    contentPadding: EdgeInsets.all(10.0),
                                    border: OutlineInputBorder(
                                        borderRadius:
                                            BorderRadius.circular(20.0))),
                                onEditingComplete: () {
                                  setState(() {});
                                },
                                keyboardType: TextInputType.name,
                              ),
                            )
                          ],
                        ),
                        SizedBox(
                          height: 15,
                        ),
                        Container(
                          padding: EdgeInsets.only(right: 10),
                          width: 400,
                          child: TextFormField(
                            controller: _email,
                            decoration: InputDecoration(
                                icon: Icon(Icons.email_sharp,
                                    color: Colors.indigo),
                                hintText: "Email",
                                contentPadding: EdgeInsets.all(10.0),
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(20.0))),
                            onEditingComplete: () {
                              setState(() {});
                            },
                            keyboardType: TextInputType.emailAddress,
                          ),
                        ),
                        SizedBox(
                          height: 15,
                        ),
                        Container(
                          padding: EdgeInsets.only(right: 10),
                          width: 400,
                          child: TextFormField(
                            obscureText: true,
                            decoration: InputDecoration(
                                icon: Icon(Icons.vpn_key_sharp,
                                    color: Colors.indigo),
                                hintText: "Contraseña",
                                contentPadding: EdgeInsets.all(10.0),
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(20.0))),
                            onEditingComplete: () {
                              setState(() {});
                            },
                            keyboardType: TextInputType.visiblePassword,
                          ),
                        ),
                        SizedBox(
                          height: 15,
                        ),
                        Row(mainAxisAlignment: MainAxisAlignment.start,children: <Widget>[
                          Container(
                            padding: EdgeInsets.only(right: 15),
                            width: MediaQuery.of(context).copyWith().size.width / 2,
                            child: TextFormField(
                              controller: _number,
                              decoration: InputDecoration(
                                  icon: Icon(Icons.phone, color: Colors.indigo),
                                  hintText: "Número de móvil",
                                  contentPadding: EdgeInsets.all(10.0),
                                  border: OutlineInputBorder(
                                      borderRadius:
                                          BorderRadius.circular(20.0))),
                              onEditingComplete: () {
                                setState(() {});
                              },
                              keyboardType: TextInputType.phone,
                            ),
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Container(
                                //color: Colors.lightBlueAccent,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(20),
                                  color: Colors.lightBlueAccent
                                ),
                                child: IconButton(
                                  color: Colors.indigo,
                                  icon: Icon(Icons.calendar_today_sharp),
                                  onPressed: () {
                                    showDatePicker(
                                      context: context,
                                      initialDate: DateTime.now(),
                                      firstDate: DateTime(2015, 8),
                                      lastDate: DateTime(2101),
                                    );
                                  },
                                ),
                              ),
                              SizedBox(width: 10,),
                              Text("Fecha nacimiento")
                            ],
                          ),
                        ]),
                        Container(
                          height: 190,
                          child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                SizedBox(
                                  height: 15,
                                ),
                          ListTile(
                            leading: Radio(
                              value: 'male',
                              groupValue: this.gender,
                              onChanged: (v) {
                                setState(() {
                                  this.gender = v;
                                });
                              },
                            ),
                            title: Text('Hombre'),
                          ),
                          ListTile(
                            leading: Radio(
                              value: 'female',
                              groupValue: this.gender,
                              onChanged: (v) {
                                setState(() {
                                  this.gender = v;
                                });
                              },
                            ),
                            title: Text('Mujer'),
                          ),
                                ListTile(
                                  leading: Radio(
                                    value: 'other',
                                    groupValue: this.gender,
                                    onChanged: (v) {
                                      setState(() {
                                        this.gender = v;
                                      });
                                    },
                                  ),
                                  title: Text('Otro'),
                                ),]
                          )
                        ),
                        Container(
                          width: double.maxFinite,
                          child: Padding(
                            padding: const EdgeInsets.only(top: 15.0),
                            // ignore: deprecated_member_use
                            child: RaisedButton(shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(20),
                            ),
                              onPressed: () {
                                Navigator.of(context).pushNamed(ListProductsPage.tag);
                              },
                              padding: EdgeInsets.all(12),
                              color: Colors.lightBlueAccent,
                              child: Text('REGISTRARSE', style: TextStyle(color: Colors.white, letterSpacing: 4)),
                            ),
                          ),
                        )
                      ],
                    ),
                  )),
            ],
          ),
        ),
      ),
    )
        /*
      Container(
        padding: EdgeInsets.symmetric(vertical: 100),
        child: SizedBox(
          height: double.maxFinite,
          width: double.maxFinite,
          child: Container(
            padding: EdgeInsets.all(10),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Row(
                  children: [
                    TextFormField(
                      controller: _name,

                    )
                  ],
                )
              ]
            ),
          ),
        ),
      ),
      */

        );
  }
}
