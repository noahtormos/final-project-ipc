import 'package:finalipc/model/product_modelData.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:finalipc/pages/shoppingCar.dart';

class ProductDetailsPage extends StatelessWidget {
  final Product product;
  List<Product> added ;

  ProductDetailsPage({@required this.product, @required added}){this.added = added;}


  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: Text("Detalles de producto"),
          backgroundColor: Colors.deepPurpleAccent,
        ),
        body: _ProductDetails(this.product, this.added),
    );
  }
}

class _ProductDetails extends StatelessWidget {
  final Product product;
  List<Product> added;

  _ProductDetails(this.product, this.added){this.added = added;}

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        _Header(product),
        _Info(product),
        _AddButton(product,added),
      ],
    );
  }
}


class _Header extends StatelessWidget {
  final Product product;

  _Header(this.product);

  @override
  Widget build(BuildContext context) {
    return Hero(tag: this.product.name, child: Container(
      margin: EdgeInsets.only(top: 10),
      padding: EdgeInsets.symmetric(horizontal: 30),
      width: double.infinity,
      height: 300.0,
      child: ClipRRect(
          borderRadius: BorderRadius.circular(25),
          child: Image.asset(
            this.product.image,
            //fit: BoxFit.cover,
          )),
    ),
    );
  }
}

class _Info extends StatelessWidget {
  final Product product;

  _Info(this.product);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 200,
      margin: EdgeInsets.only(top: 10, right: 10, left: 10),
      padding: EdgeInsets.symmetric(horizontal: 30),
      width: double.infinity,
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.all(Radius.circular(25))),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          SizedBox(
            height: 20,
          ),
          Text("${this.product.name}"),
          SizedBox(
              height: 10),
          Text("${this.product.price} €"),
          SizedBox(height: 10,),
          Text("${this.product.description}"),
        ],
      ),
    );
  }
}


class _AddButton extends StatelessWidget {
  final Product product;
  List<Product> added;

  _AddButton(this.product,this.added){this.added = added;}

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 30),
      child: SizedBox(
          height: 50,
          width: double.maxFinite,
          child: ElevatedButton(
            onPressed: () {
              addToCart(product, context, added);
            },
            style: ButtonStyle(
              backgroundColor: MaterialStateProperty.all<Color>(Colors.indigo),
            ), child: Text("ADD"),
            ),
      ),
    );
  }

  void addToCart(Product product, BuildContext context, List<Product> added) {
    Navigator.push(context, MaterialPageRoute(builder: (context) => shoppingCar(product: product, allProducts:added,)));
  }
}