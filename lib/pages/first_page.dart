import 'package:finalipc/pages/register_page.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'login_page.dart';

class FirstPage extends StatelessWidget {
  static String tag = 'first-page';

  @override
  Widget build(BuildContext context) {
    return Container(
      child: FutureBuilder(
          future: loadingApp(context),
          builder: (context, snapshot) {
            if (!snapshot.hasData)
              return Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                    height: 150,
                    width: 150,
                    child: Image.asset(
                      "assets/img/logo.png",
                    ),
                  ),
                  Divider(),
                  CircularProgressIndicator()
                ],
              );

            return Scaffold(
              backgroundColor: Colors.indigo,
              body: SafeArea(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [_Header(), _Body(), _Footer()],
                ),
              ),
            );
          }),
    );
  }
}

class _Header extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.maxFinite,
      height: 160,
      padding: EdgeInsets.only(left: 10),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              Container(
                padding: EdgeInsets.all(10),
                  height: 50,
                  width: 50,
                  child: Image.asset(
                    "assets/img/logo.png",
                  )),
              Text(
                "PC CON PONENTES",
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 22,
                    fontWeight: FontWeight.bold,
                    letterSpacing: 2),
              ),
            ],
          ),
          Container(
            padding: EdgeInsets.only(right: 200, left: 10),
            child: Text(
                "Tu tienda de confianza para comenzar a vivir la experiencia GAMER",
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 18,
                )),
          )
        ],
      ),
    );
  }
}

class _Body extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.maxFinite,
      height: 250,
      child: Image.asset("assets/img/niño-gamer.gif", height: 125.0,
        width: 125.0,),
    );
  }
}

class _Footer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.maxFinite,
      height: 180,
      padding: EdgeInsets.only(bottom: 40),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          SizedBox(
            width: 200,
            // ignore: deprecated_member_use
            child: RaisedButton(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20),
              ),
              onPressed: () {
                Navigator.of(context).pushNamed(LoginPage.tag);
              },
              padding: EdgeInsets.all(12),
              color: Colors.lightBlueAccent,
              child: Text('INICIAR SESIÓN',
                  style: TextStyle(color: Colors.white, letterSpacing: 4)),
            ),
          ),
          SizedBox(
            width: 200,
            // ignore: deprecated_member_use
            child: RaisedButton(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20),
              ),
              onPressed: () {
                Navigator.of(context).pushNamed(RegisterPage.tag);
              },
              padding: EdgeInsets.all(12),
              color: Colors.lightBlueAccent,
              child: Text('REGISTRARSE',
                  style: TextStyle(color: Colors.white, letterSpacing: 4)),
            ),
          ),
        ],
      ),
    );
  }
}

Future<String> loadingApp(BuildContext context) {
  return Future.delayed(Duration(seconds: 3), () {
    return "https://media.giphy.com/media/1lBjBpMwgI8PBZKov0/giphy.gif";
  });
}
