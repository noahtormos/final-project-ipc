class Products {
  List<Product> products;

  Products({List<Product> products}) {
    this.products = products;
  }

  Products.fromJson(Map<String, dynamic> json) {
    if (json['products'] != null) {
      products = <Product>[];
      json['products'].forEach((v) {
        products.add(new Product.fromJson(v));
      });
    }
  }

  String get name => this.name;
  double get price => this.price;
  String get image => this.image;
  String get description => this.description;
}

class Product {
  String name;
  double price;
  String image;
  String description;

  Product({String name, double price, String image, String description}) {
    this.name = name;
    this.price = price;
    this.image = image;
    this.description = description;
  }

  Product.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    price = json['price'];
    image = json['image'];
    description = json['description'];
  }
}